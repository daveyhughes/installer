#!/bin/bash

# Contents of the ctags file
ctags="\
#!/bin/sh
set -e
PATH=\"/usr/local/bin:$PATH\"
dir=\"`git rev-parse --git-dir`\"
trap 'rm -f \"$dir/$$.tags\"' EXIT
git ls-files | \\
  ctags --tag-relative -L - -f\"$dir/$$.tags\" --languages=-javascript,sql
mv \"$dir/$$.tags\" \"$dir/tags\""

# Contents of the hooks files.
hooks="\
#!/bin/sh
.git/hooks/ctags >/dev/null 2>&1 &"

hook_rewrite="\
#!/bin/sh
case \"$1\" in
  rebase) exec .git/hooks/post-merge ;;
esac"

cont () {
  response="n"
  while [[ ! $response =~ ^(yes|y)$ ]]; do
    read -r -p "Continue? [y] " response
    response=${response,,} # tolower
  done
}

# argument $1 is used for vim version
# ubuntu -> "vim.nox-py2"
# macOS -> "vim"
gitconfig () {
  git config --global user.username "davey"
  git config --global user.email "davidralphhughes@college.harvard.edu"
  git config --global core.editor $1
  git config --global push.default simple

  # template for doing ctags hooks
  git config --global init.templatedir '~/.git_template'
  cd $HOME
  mkdir -p ~/.git_template/hooks
  cd $HOME/.git_template/hooks/

  # Create hook files
  echo "$ctags" > ctags
  echo "$hooks" | tee post-commit post-merge > post-checkout
  echo "$hook_rewrite" > post-rewrite

  # Make them executable.
  chmod 766 *
}

installVim () {
  if [[ doVim == true ]]; then
    cd $HOME/
    git clone git@bitbucket.org:daveyhughes/.vim.git
    cd $HOME/.vim/
    git submodule update --init --recursive

    if [[ doYCM == true ]]; then
      cd $HOME/.vim/bundle/YouCompleteMe/
      ./install.py --clang-completer
    fi
  fi
}

# argument $1 is used as the .zshrc version to be copied
# possible values are:
#   macOS
#   ubuntu
installOMZ () {
  if [[ doOMZ == true ]]; then
    cd $HOME/
    sh -c "$(curl -fsSL \
      https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    cd $HOME/.oh-my-zsh/
    git submodule add git@bitbucket.org:daveyhughes/omz-davey.git
    mv $HOME/.zshrc $HOME/.zshrc.old
    cp $HOME/.oh-my-zsh/omz-davey/.zshrc.$1 $HOME/.zshrc
    cp $HOME/.oh-my-zsh/omz-davey/davey.zsh-theme $HOME/.oh-my-zsh/themes/
  fi
}

# optional parameters
#
# -a -> all
# -e -> essential
# -j -> java8
# -o -> oh-my-zsh
# -t -> texlive-full
# -v -> vim
# -y -> YouCompleteMe

doJava=false
doOMZ=false
doTexLive=false
doVim=false
doYCM=false

while getopts ":aejotvy" opt; do
  case $opt in
    a)
      doJava=true
      doOMZ=true
      doVim=true
      doTexLive=true
      doVim=true
      doYCM=true
      ;;
    e)
      doOMZ=true
      doVim=true
      doYCM=true
      ;;
    j) doJava=true ;;
    o) doOMZ=true ;;
    t) doTexLive=true ;;
    v) doVim=true ;;
    y) doYCM=true ;;
    \?) echo "Invalid option: -$OPTARG" ;;
  esac
done

ssh-keygen -t rsa -N "" -f $HOME/.ssh/id_rsa
echo "Here is your public key. Copy this and put where needed."
cat $HOME/.ssh/id_rsa.pub
cont

case "$OSTYPE" in
  linux*)
    OS=$(lsb_release -si)
    case "$OS" in
      Ubuntu)
        # initial packages
        sudo add-apt-repository ppa:jonathonf/python-3.6 -y
        sudo add-apt-repository ppa:jonathonf/vim -y
        if [[ doJava == true ]]; then
          sudo add-apt-repository ppa:webupd8team/java -y
        fi
        sudo apt-get update
        sudo apt-get install -y \
          git zsh guake cmake vim-nox-py2 build-essential \
          python-dev python-pip python3.6 python3-pip python3-dev \
          exuberant-ctags

        if [[ doJava == true ]]; then
          sudo apt-get install -y oracle-java8-installer
        fi

        if [[ doTexLive == true ]]; then
          sudo apt-get install -y texlive-full
        fi

        # git
        gitconfig "vim.nox-py2"

        # vim
        installVim

        # oh-my-zsh
        installOMZ ubuntu

        # guake
        cd $HOME/
        mkdir temp
        cd $HOME/temp/
        wget https://github.com/coolwanglu/guake-colors-solarized/archive/gray.zip
        unzip gray.zip
        cd $HOME/temp/guake-colors-solarized-gray/
        ./set_dark.sh
        cd $HOME/temp/
        rm -rf gray.zip guake-colors-solarized-gray/

        ;;
      *)
        echo "unknown: $OS" ;;
    esac
    ;;

  darwin*)
    /usr/bin/ruby -e \
      "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

    brew tap homebrew/dupes

    brew install \
      binutils cmake coreutils diffutils findutils gcc gdb git \
      htop less make moreutils openssh python python3 thefuck tmux wget zsh \
      zsh-completions ctags
    brew install grep --with-default-names
    brew install macvim
    brew cask install iterm2

    compaudit | xargs chmod g-w

    # git
    gitconfig "vim"

    # vim
    installVim

    # oh-my-zsh
    installOMZ macOS

    ;;
  *)
    echo "unknown: $OSTYPE" ;;
esac
